package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class WeatherDurationMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_weather_remaining_ticks";

    public static final WorldMetric INSTANCE = new WeatherDurationMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The remaining ticks for the current weather condition.")
        .labelNames("world")
        .create();

    private WeatherDurationMetric()
    {
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.getWeatherDuration());
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
