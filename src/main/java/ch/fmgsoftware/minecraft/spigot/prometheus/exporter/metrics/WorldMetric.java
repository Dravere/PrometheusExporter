package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import org.bukkit.World;


public interface WorldMetric
    extends Metric
{
    void update(World world);
}
