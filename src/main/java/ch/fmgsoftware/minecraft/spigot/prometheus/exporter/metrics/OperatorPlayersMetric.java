package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;


public class OperatorPlayersMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_operator_players";

    public static final BasicMetric INSTANCE = new OperatorPlayersMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The amount of players with operator permissions.")
        .create();

    private OperatorPlayersMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update()
    {
        GAUGE.set(Bukkit.getOperators().size());
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
