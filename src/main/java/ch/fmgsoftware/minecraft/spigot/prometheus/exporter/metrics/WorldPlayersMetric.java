package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class WorldPlayersMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_world_players";

    public static final WorldMetric INSTANCE = new WorldPlayersMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The amount of player per world.")
        .labelNames("world")
        .create();

    private WorldPlayersMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.getPlayers().size());
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
