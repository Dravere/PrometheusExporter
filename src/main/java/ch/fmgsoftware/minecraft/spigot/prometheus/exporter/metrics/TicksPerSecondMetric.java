package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import ch.fmgsoftware.minecraft.spigot.prometheus.exporter.PrometheusExporter;
import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;


public class TicksPerSecondMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_tps";

    public static final BasicMetric INSTANCE = new TicksPerSecondMetric();

    private static final long TICKS_PER_MINUTE = 20 * 60;

    private static final Gauge GAUGE = Gauge.build().name(NAME).help("The ticks per second on the server.").create();

    private long m_lastTpsMeasurement;

    private TicksPerSecondMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.set(Double.NaN);
        GAUGE.register();

        m_lastTpsMeasurement = System.currentTimeMillis();

        PrometheusExporter plugin = JavaPlugin.getPlugin(PrometheusExporter.class);
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            long currentTime = System.currentTimeMillis();
            double elapsedTime = (currentTime - m_lastTpsMeasurement) / 1000.0;
            m_lastTpsMeasurement = currentTime;
            GAUGE.set(TICKS_PER_MINUTE / elapsedTime);
        }, TICKS_PER_MINUTE, TICKS_PER_MINUTE);
    }

    @Override
    public void update()
    {
        // Ignore, since we already update the gauge through the scheduler.
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
