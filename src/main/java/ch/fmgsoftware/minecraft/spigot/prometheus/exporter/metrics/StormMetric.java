package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class StormMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_storm";

    public static final WorldMetric INSTANCE = new StormMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("If there is a storm in the world 1; otherwise 0.")
        .labelNames("world")
        .create();

    private StormMetric()
    {
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.hasStorm() ? 1 : 0);
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
