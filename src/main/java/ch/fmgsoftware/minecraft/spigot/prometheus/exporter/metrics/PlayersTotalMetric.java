package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;


public class PlayersTotalMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_players_total";

    public static final BasicMetric INSTANCE = new PlayersTotalMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The total amount of players that ever set foot on this server.")
        .create();

    private PlayersTotalMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update()
    {
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
