package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;


public class OnlinePlayersMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_online_players";

    public static final BasicMetric INSTANCE = new OnlinePlayersMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The amount of players that are currently online.")
        .create();

    private OnlinePlayersMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update()
    {
        GAUGE.set(Bukkit.getOnlinePlayers().size());
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
