package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class WorldFullTimeMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_game_full_time";

    public static final WorldMetric INSTANCE = new WorldFullTimeMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The current game full time in each world.")
        .labelNames("world")
        .create();

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.getFullTime());
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
