package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class LoadedChunksMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_loaded_chunks";

    public static final WorldMetric INSTANCE = new LoadedChunksMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The amount of chunks loaded per world.")
        .labelNames("world")
        .create();

    private LoadedChunksMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.getLoadedChunks().length);
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
