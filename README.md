# Prometheus Exporter for Minecraft Spigot

This spigot plugin provides metrics about the server for a Prometheus instance. The metrics are grouped into categories that can be turned on and off. The port of the HTTP server providing the data and the path can be configured.

## Installation

Just download the latest release as ZIP-file and drop the entire content into your `plugins` folder of the Minecraft server. Make sure that the `libs` folder is also extracted into your `plugins` folder. Otherwise the plugin won't work.

## Metrics

The metrics are categories in the following groups.

| Name | Default | Description |
|------|---------|-------------|
| jvm | on | Metrics about the Java Virtual Machine. |
| basics | on | Some basic metrics about the server. |
| players | on | Information about the amount of players on the server, in the worlds and in total. |
| entities | on | The amount of entities per world. |
| environment | off | How many worlds are loaded. |
| plugins | off | The amount of loaded plugins. |
| bans | off | The amount of banned IPs and players. |
| weather | off | Information about weather condition and remaining duration. |
| worldTime | off | Information about the current world time and full time. |
| onlineMode | off | If the server is in online mode or not. |
| difficulty | off | What difficulty setting a world has set. |
| autoSave | off | If a world has auto save activated. |

Every group is further described in their corresponding chapters of this document.

### JVM

By default on.

JVM hotspot metrics are provided. Have a [look](https://github.com/prometheus/client_java) at the official Prometheus Java library for this.

### Basics

By default on.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_tps | - | The server ticks per second (average over one minute). The first value will be available one minute after activation. In the time before the value will be set to NaN. |
| minecraft_server_loaded_chunks | world | The amount of chunks loaded per world. |


### Players

By default on.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_online_players | - | The amount of players that are currently online. |
| minecraft_server_players_total | - | The total amount of players that ever set foot on this server. |
| minecraft_server_max_players | - | The maximum amount of players allowed on the server. |
| minecraft_server_operator_players | - | The amount of players with operator rights. |
| minecraft_server_world_players | world | The amount of players per world. |


### Entities

By default on.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_entities | world | The amount of entities per world. |
| minecraft_server_living_entities | world | The amount of living entities per world. |


### Environments

By default off.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_worlds | environment | The loaded worlds ('NORMAL', 'NETHER', 'THE_END'). |


### Plugins

By default off.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_loaded_plugins | name | If the loaded plugin is enabled 1; otherwise 0. |


### Bans

By default off.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_banned_ips | - | The amount of banned IPs. |
| minecraft_server_banned_players | - | The amount of banned players. |


### Weather

By default off.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_storm | world | If it is storming in the world 1; otherwise 0. |
| minecraft_server_thundering | world | If it is thundering in the world 1; otherwise 0. |
| minecraft_server_thunder_remaining_ticks | world | The amount of remaining ticks in the current thunderstorm. |
| minecraft_server_weather_remaining_ticks | world | The amount of remaining ticks in the current weather condition. |

### World Time

By default off.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_game_time | world | The current time of day in the world (24 * 1000). |
| minecraft_server_game_full_time | world | The full game time of the world. |

### Online Mode

By default off.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_online_mode | - | If the server is in online mode 1; otherwise 0. |


### Difficulty

By default off.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_difficulty | world | The difficulty setting in the world (0: peaceful, 1: easy, 2: normal, 3: hard, NaN: unknown). |


### Auto Save

By default off.

| Metric name | Labels | Description |
|-------------|--------|-------------|
| minecraft_server_auto_save | world | If the world is auto saved 1; otherwise 0. |
